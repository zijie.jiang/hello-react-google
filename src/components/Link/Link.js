import React from "react";
import './Link.less'

const Link = (props) => {
    return (
        <nav>
            <ul>
                {
                    props.value.map((item, index) => <li key={index}>{item}</li>)
                }
            </ul>
        </nav>
    );
};


export default Link;
