import React from 'react';
import './App.less';
import Menu from "../Menu/Menu";
import Search from '../Search/Search';
import Footer from "../Footer/Footer";

const App = () => {
  return (
    <div className='App'>
      <Menu />
      <Search />
      <Footer />
    </div>
  );
};

export default App;
