import React from 'react';
import './Search.less';
import {MdMic, MdSearch} from "react-icons/md";
import Button from "../Button/Button";
import Language from "../Language/Language";

const Search = () => {
    return (
        <section className='search'>

            <header>
                <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
                     alt="Google Logo"/>
            </header>

            <article>
                <MdSearch className='search-icon'/>
                <input type="text"/>
                <MdMic className='voice-icon'/>
            </article>

            <section>
                <Button value='Google 搜索'/>
                <Button value='手气不错'/>
            </section>

            <section className='provide'>
                <span>google 提供: </span>
                <Language value='中文'/>
                <Language value='English'/>
            </section>

        </section>
    );
};

export default Search;
