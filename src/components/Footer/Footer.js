import React from "react";
import './Footer.less'
import Link from "../Link/Link";

const Footer = () => {
    const copyRightLink = ['隐私权', '条款', '设置'];
    const externalLink = ['广告', '商务', 'Google 大全', 'Google 搜索的运作方式'];
    return (
        <footer>
            <section>
                香港
            </section>
            <section>
                <Link value={externalLink}/>
                <Link value={copyRightLink}/>
            </section>
        </footer>
    );
};

export default Footer;
